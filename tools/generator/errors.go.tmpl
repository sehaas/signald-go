package {{.Version}}

// {{ .Banner }}

import (
    "encoding/json"
    "fmt"

    client_protocol "gitlab.com/signald/signald-go/signald/client-protocol"
)

func mkerr(response client_protocol.BasicResponse) error {
	switch response.ErrorType { {{ range $structName, $type := .Types }}
	case "{{ $structName }}":
		result := {{ $structName }}{}
		err := json.Unmarshal(response.Error, &result)
		if err != nil {
			return err
		}
		return result{{ end }}
    default:
        return fmt.Errorf("unexpected response type from signald: %s: %s", response.ErrorType, string(response.Error))
	}
}

{{ range $structName, $type := .Types }}{{if ne $type.Doc ""}}// {{$structName}}: {{$type.Doc}}{{end}}
type {{ $structName }} struct {
{{if $type.Request}}  Request{{end}}
{{ range $fieldName, $field := $type.Fields }}{{ $field.FieldName }} {{if $field.List}}[]{{end}}{{ $field.Type }} `json:"{{$fieldName}},omitempty" yaml:"{{$fieldName}},omitempty"`{{if ne $field.Doc ""}} // {{$field.Doc}}{{end}}
{{ end }}
}

func (e {{ $structName }}) Error() string {
    return e.Message
}

{{ end }}
