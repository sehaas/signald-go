package db

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"os/user"
	"strings"
	"time"

	"github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
)

type Migration struct {
	InstalledRank int
	Version       string
	Description   string
	Script        string
	Checksum      int
}

var (
	migrations = []Migration{
		{InstalledRank: 1, Version: "1", Description: "create tables", Script: "V1__create_tables.sql", Checksum: -1247750968},
		{InstalledRank: 2, Version: "12", Description: "create contacts table", Script: "V12__create_contacts_table.sql", Checksum: -852729911},
		{InstalledRank: 3, Version: "13", Description: "recipient registration status", Script: "V13__recipient_registration_status.sql", Checksum: 405376321},
		{InstalledRank: 4, Version: "14", Description: "multiple identity keys per account", Script: "V14__multiple_identity_keys_per_account.sql", Checksum: -1635788950},
		{InstalledRank: 5, Version: "15", Description: "profiles tables", Script: "V15__profiles_tables.sql", Checksum: 809686180},
	}

	sqlitePath  string
	postgresURL string
	MoveCmd     = &cobra.Command{
		Use:   "db-move pg-url [sqlite-path]",
		Short: "move a signald database from sqlite to postgres",
		Long: `move a signald sqlite database into a postgres database.
	If sqlite-path is not specified, the default (~/.config/signald/signald.db) will be used.

	Please note that signald must NOT be running while this command runs.

	After the data is moved, the sqlite file will be deleted`,
		Annotations: map[string]string{common.AnnotationNoSocketConnection: "true"},
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("at least one argument required")
			}
			postgresURL = args[0]
			if len(args) > 1 {
				sqlitePath = args[1]
			} else {
				usr, _ := user.Current()
				sqlitePath = fmt.Sprintf("%s/.config/signald/signald.db", usr.HomeDir)
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			source, err := sql.Open("sqlite3", sqlitePath)
			if err != nil {
				return err
			}
			defer source.Close()

			if err := source.Ping(); err != nil {
				log.Println("error connecting to source database")
				return err
			}

			if err := verifyMigration(source); err != nil {
				return err
			}

			dest, err := sql.Open("postgres", postgresURL)
			if err != nil {
				return err
			}
			defer dest.Close()

			if err := dest.Ping(); err != nil {
				log.Println("error connecting to destination database")
				return err
			}

			if err := createSchema(dest); err != nil {
				log.Println("error creating schema in postgres")
				return err
			}
			log.Println("created schema")

			if err := moveAccounts(source, dest); err != nil {
				log.Println("error moving accounts table")
				return err
			}
			log.Println("moved accounts table")

			if err := moveRecipients(source, dest); err != nil {
				log.Println("error moving recipients table")
				return err
			}
			log.Println("moved recipients table")

			if err := movePrekeys(source, dest); err != nil {
				log.Println("error moving prekeys table")
				return err
			}
			log.Println("moved prekeys table")

			if err := moveSessions(source, dest); err != nil {
				log.Println("error moving sessions table")
				return err
			}
			log.Println("moved sessions table")

			if err := moveSignedPrekeys(source, dest); err != nil {
				log.Println("error moving signed prekeys table")
				return err
			}
			log.Println("moved signed prekeys table")

			if err := moveIdentityKeys(source, dest); err != nil {
				log.Println("error moving identity keys table")
				return err
			}
			log.Println("moved identity keys table")

			if err := moveAccountData(source, dest); err != nil {
				log.Println("error moving account data")
				return err
			}
			log.Println("moved account data table")

			if err := movePendingAccountData(source, dest); err != nil {
				log.Println("error moving pending account data tabe")
				return err
			}
			log.Println("moved pending account data table")

			if err := moveSenderKeys(source, dest); err != nil {
				log.Println("error moving sender keys table")
				return err
			}
			log.Println("moved sender keys table")

			if err := moveSenderKeyShared(source, dest); err != nil {
				log.Println("error moving sender key shared table")
				return err
			}
			log.Println("moved sender key shared table")

			if err := moveGroups(source, dest); err != nil {
				log.Println("error moving groups table")
				return err
			}
			log.Println("moved groups table")

			if err := moveGroupCredentials(source, dest); err != nil {
				log.Println("error moving group credentials table")
				return err
			}
			log.Println("moved group credentials table")

			if err := moveContacts(source, dest); err != nil {
				log.Println("error moving group credentials table")
				return err
			}
			log.Println("moved contacts table")

			if err := moveProfileKeys(source, dest); err != nil {
				log.Println("error moving profile keys table")
				return err
			}
			log.Println("moved profile keys table")

			if err := moveProfiles(source, dest); err != nil {
				log.Println("error moving profiles tables")
				return err
			}
			log.Println("moved profiles tables")

			if err := moveProfileCapabilities(source, dest); err != nil {
				log.Println("error moving profile capabilities tables")
				return err
			}
			log.Println("moved profile capabilities tables")

			if err := moveProfileBadges(source, dest); err != nil {
				log.Println("error moving profile badges tables")
				return err
			}
			log.Println("moved profile badges tables")

			if err := os.Remove(sqlitePath); err != nil {
				log.Println("error deleting sqlite file")
				return err
			}
			log.Println("sqlite file deleted, your data is now in postgres :)")
			return nil
		},
	}
)

func verifyMigration(source *sql.DB) error {
	// Lower bound of the database state.
	rows, err := source.Query("SELECT version FROM flyway_schema_history ORDER BY installed_rank DESC LIMIT 1")
	if err != nil {
		return err
	}
	defer rows.Close()

	if !rows.Next() {
		return errors.New("source database is not up to date! Please update signald and start it to move all data into sqlite before moving data to postgres")
	}

	var version string
	err = rows.Scan(&version)
	if err != nil {
		return err
	}

	expectedMigrationVersion := migrations[len(migrations)-1].Version
	if version != expectedMigrationVersion {
		return fmt.Errorf("source database must be on migration %s (found %s instead). Please update signald, or file an issue if the migrations are out of date", expectedMigrationVersion, version)
	}

	return nil
}

func createSchema(dest *sql.DB) error {
	_, err := dest.Exec(pgScheme)
	if err != nil {
		return err
	}

	for _, migration := range migrations {
		_, err = dest.Exec(`
        INSERT INTO flyway_schema_history
               (installed_rank, version, description, type, script, checksum, installed_by, execution_time, success)
        VALUES ($1, $2, $3, 'SQL', $4, $5, current_user, 0, true)
        `,
			migration.InstalledRank, migration.Version, migration.Description, migration.Script, migration.Checksum,
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func moveAccounts(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT uuid, e164, server FROM accounts")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			e164        string
			server      uuid.UUID
		)
		err = rows.Scan(&accountUUID, &e164, &server)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_accounts (uuid, e164, server) VALUES ($1, $2, $3, $4)", accountUUID, e164, server)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveRecipients(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT rowid, account_uuid, uuid, e164, registered FROM recipients")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			rowID         int64
			accountUUID   uuid.UUID
			recipientUUID uuid.NullUUID
			e164          sql.NullString
			registered    bool
		)
		err = rows.Scan(&rowID, &accountUUID, &recipientUUID, &e164, &registered)
		if err != nil {
			return err
		}

		if e164.Valid && !strings.HasPrefix(e164.String, "+") {
			log.Println("corrupt e164 found, setting to null")
			e164.Valid = false
			e164.String = ""
		}

		_, err = dest.Exec("INSERT INTO signald_recipients (rowid, account_uuid, uuid, e164, registered) VALUES ($1, $2, $3, $4, $5)", rowID, accountUUID, recipientUUID, e164, registered)
		if err != nil {
			return err
		}
	}

	// start new rowids one above the current max value
	_, err = dest.Exec("SELECT setval(pg_get_serial_sequence('signald_recipients', 'rowid'), (SELECT MAX(rowid) FROM signald_recipients)+1)")
	if err != nil {
		return err
	}
	return nil
}

func movePrekeys(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, id, record FROM prekeys")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			id          int64
			record      []byte
		)
		err = rows.Scan(&accountUUID, &id, &record)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_prekeys (account_uuid, id, record) VALUES ($1, $2, $3)", accountUUID, id, record)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveSessions(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, recipient, device_id, record FROM sessions")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			recipient   int64
			deviceID    int64
			record      []byte
		)
		err = rows.Scan(&accountUUID, &recipient, &deviceID, &record)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_sessions (account_uuid, recipient, device_id, record) VALUES ($1, $2, $3, $4)", accountUUID, recipient, deviceID, record)

		if err != nil {
			if pqErr, ok := err.(*pq.Error); ok {
				if pqErr.Constraint == "signald_sessions_recipient_fkey" {
					log.Println("failed to import session from non-existent recipient, ignoring")
				} else {
					return err
				}
			} else {
				return err
			}
		}
	}
	return nil
}

func moveSignedPrekeys(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, id, record FROM signed_prekeys")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			id          int64
			record      []byte
		)
		err = rows.Scan(&accountUUID, &id, &record)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_signed_prekeys (account_uuid, id, record) VALUES ($1, $2, $3)", accountUUID, id, record)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveIdentityKeys(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, recipient, identity_key, trust_level, added FROM identity_keys")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			recipient   int64
			identityKey []byte
			trustLevel  string
			added       time.Time
		)
		err = rows.Scan(&accountUUID, &recipient, &identityKey, &trustLevel, &added)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_identity_keys (account_uuid, recipient, identity_key, trust_level, added) VALUES ($1, $2, $3, $4, $5)", accountUUID, recipient, identityKey, trustLevel, added)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveAccountData(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, key, value FROM account_data")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			key         string
			value       []byte
		)
		err = rows.Scan(&accountUUID, &key, &value)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_account_data (account_uuid, key, value) VALUES ($1, $2, $3)", accountUUID, key, value)
		if err != nil {
			return err
		}
	}
	return nil
}

func movePendingAccountData(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT username, key, value FROM pending_account_data")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			username string
			key      string
			value    []byte
		)
		err = rows.Scan(&username, &key, &value)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_pending_account_data (username, key, value) VALUES ($1, $2, $3)", username, key, value)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveSenderKeys(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, address, device, distribution_id, record, created_at FROM sender_keys")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID    uuid.UUID
			address        string
			device         int64
			distributionID uuid.UUID
			record         []byte
			createdAt      int64
		)
		err = rows.Scan(&accountUUID, &address, &device, &distributionID, &record, &createdAt)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_sender_keys (account_uuid, address, device, distribution_id, record, created_at) VALUES ($1, $2, $3, $4, $5, $6)", accountUUID, address, device, distributionID, record, time.Unix(createdAt, 0))
		if err != nil {
			if pqErr, ok := err.(*pq.Error); ok {
				if pqErr.Constraint == "signald_sender_keys_account_uuid_fkey" {
					log.Println("failed to import sender keys from non-existent account, ignoring")
				} else {
					return err
				}
			} else {
				return err
			}
		}
	}
	return nil
}

func moveSenderKeyShared(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, distribution_id, address, device FROM sender_key_shared")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID    uuid.UUID
			distributionID uuid.UUID
			address        string
			device         int64
		)
		err = rows.Scan(&accountUUID, &distributionID, &address, &device)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_sender_key_shared (account_uuid, distribution_id, address, device) VALUES ($1, $2, $3, $4)", accountUUID, distributionID, address, device)
		if err != nil {
			if pqErr, ok := err.(*pq.Error); ok {
				if pqErr.Constraint == "signald_sender_key_shared_account_uuid_fkey" {
					log.Println("failed to import sender keys shared from non-existent account, ignoring")
				} else if pqErr.Constraint == "signald_sender_key_shared_pkey" {
					log.Println("failed to import duplicate sender key shared entry, ignoring")
				}
			} else {
				return err
			}
		} else {
			return err
		}
	}
	return nil
}

func moveGroups(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT rowid, account_uuid, group_id, master_key, revision, last_avatar_fetch, distribution_id, group_info FROM groups")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			rowID           int64
			accountUUID     uuid.UUID
			groupID         []byte
			masterKey       []byte
			revision        int64
			lastAvatarFetch int64
			distributionID  *uuid.UUID
			groupInfo       []byte
		)
		err = rows.Scan(&rowID, &accountUUID, &groupID, &masterKey, &revision, &lastAvatarFetch, &distributionID, &groupInfo)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_groups (rowid, account_uuid, group_id, master_key, revision, last_avatar_fetch, distribution_id, group_info) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)", rowID, accountUUID, groupID, masterKey, revision, lastAvatarFetch, distributionID, groupInfo)
		if err != nil {
			return err
		}
	}

	// start new rowids one above the current max value
	_, err = dest.Exec("SELECT setval(pg_get_serial_sequence('signald_groups', 'rowid'), (SELECT MAX(rowid) FROM signald_groups)+1)")
	if err != nil {
		return err
	}
	return nil
}

func moveGroupCredentials(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, date, credential FROM group_credentials")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			date        int64
			credential  []byte
		)
		err = rows.Scan(&accountUUID, &date, &credential)
		if err != nil {
			return err
		}
		_, err = dest.Exec("INSERT INTO signald_group_credentials (account_uuid, date, credential) VALUES ($1, $2, $3)", accountUUID, date, credential)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveContacts(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, recipient, name, color, profile_key, message_expiration_time, inbox_position FROM contacts")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID             uuid.UUID
			recipient               int64
			name                    sql.NullString
			color                   sql.NullString
			profile_key             []byte
			message_expiration_time sql.NullInt64
			inbox_position          sql.NullInt64
		)
		err = rows.Scan(&accountUUID, &recipient, &name, &color, &profile_key, &message_expiration_time, &inbox_position)
		if err != nil {
			return err
		}
		_, err = dest.Exec(`
            INSERT INTO signald_contacts
                        (account_uuid, recipient, name, color, profile_key, message_expiration_time, inbox_position)
                 VALUES ($1, $2, $3, $4, $5, $6, $7)
        `, accountUUID, recipient, name, color, profile_key, message_expiration_time, inbox_position)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveProfileKeys(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, recipient, profile_key, profile_key_credential, request_pending, unidentified_access_mode FROM profile_keys")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID              uuid.UUID
			recipient                int64
			profile_key              []byte
			profile_key_credential   []byte
			request_pending          bool
			unidentified_access_mode int
		)
		err = rows.Scan(&accountUUID, &recipient, &profile_key, &profile_key_credential, &request_pending, &unidentified_access_mode)
		if err != nil {
			return err
		}
		_, err = dest.Exec(`
            INSERT INTO signald_profile_keys
                        (account_uuid, recipient, profile_key, profile_key_credential, request_pending, unidentified_access_mode)
                 VALUES ($1, $2, $3, $4, $5, $6, $7)
        `, accountUUID, recipient, profile_key, profile_key_credential, request_pending, unidentified_access_mode)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveProfiles(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, recipient, last_update, given_name, family_name, about, emoji, payment_address, badges FROM profiles")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID     uuid.UUID
			recipient       int64
			last_update     int64
			given_name      string
			family_name     string
			about           string
			emoji           string
			payment_address []byte
			badges          string
		)
		err = rows.Scan(&accountUUID, &recipient, &last_update, &given_name, &family_name, &about, &emoji, &payment_address, &badges)
		if err != nil {
			return err
		}
		_, err = dest.Exec(`
            INSERT INTO signald_profiles
                        (account_uuid, recipient, last_update, given_name, family_name, about, emoji, payment_address, badges)
                 VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        `, accountUUID, recipient, last_update, given_name, family_name, about, emoji, payment_address, badges)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveProfileCapabilities(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, recipient, storage, gv1_migration, sender_key, announcement_group, change_number, stories FROM profile_capabilities")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID        uuid.UUID
			recipient          int64
			storage            bool
			gv1_migration      bool
			sender_key         bool
			announcement_group bool
			change_number      bool
			stories            bool
		)
		err = rows.Scan(&accountUUID, &recipient, &storage, &gv1_migration, &sender_key, &announcement_group, &change_number, &stories)
		if err != nil {
			return err
		}
		_, err = dest.Exec(`
            INSERT INTO signald_profile_capabilities
                        (account_uuid, recipient, storage, gv1_migration, sender_key, announcement_group, change_number, stories)
                 VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        `, accountUUID, recipient, storage, gv1_migration, sender_key, announcement_group, change_number, stories)
		if err != nil {
			return err
		}
	}
	return nil
}

func moveProfileBadges(source *sql.DB, dest *sql.DB) error {
	rows, err := source.Query("SELECT account_uuid, id, category, name, description, sprite6 FROM profile_badges")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountUUID uuid.UUID
			id          string
			category    string
			name        string
			description string
			sprite6     string
		)
		err = rows.Scan(&accountUUID, &id, &category, &name, &description, &sprite6)
		if err != nil {
			return err
		}
		_, err = dest.Exec(`
            INSERT INTO signald_profile_badges
                        (account_uuid, id, category, name, description, sprite6)
                 VALUES ($1, $2, $3, $4, $5, $6)
        `, accountUUID, id, category, name, description, sprite6)
		if err != nil {
			return err
		}
	}
	return nil
}
